package com.citi.training.products.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.citi.training.products.model.Product;
import com.citi.training.products.repo.ProductRepository;

@Component
public class Demo implements ApplicationRunner {

    @Autowired
    private ProductRepository productRepo;

    public void run(ApplicationArguments appArgs) {
        Product[] createdProducts =
                    {new Product(1, "Beans", 99.99),
                     new Product(2, "Eggs", 1.50),
                     new Product(3, "Ham", 5000.29)};
 
        for(Product thisProduct: createdProducts) {
            productRepo.save(thisProduct);
        }

        System.out.println("All Products in the repository:");
        for(Product thisProduct: productRepo.findAll()) {
            System.out.println(thisProduct);
        }
    }

    public void setProductRepo(ProductRepository productRepo) {
        this.productRepo = productRepo;
    }
}
