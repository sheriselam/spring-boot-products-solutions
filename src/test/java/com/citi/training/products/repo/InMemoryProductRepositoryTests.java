package com.citi.training.products.repo;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.citi.training.products.model.Product;

public class InMemoryProductRepositoryTests {

    private InMemoryProductRepository productRepo;

    private int testId = 677;
    private String testName = "Jam";
    private double testPrice = 3999.99;

    @Before
    public void setup() {
        productRepo = new InMemoryProductRepository();
    }

    @Test
    public void test_InMemProductRepo_saveAndFindById() {
        Product testProduct = new Product(testId, testName, testPrice);

        productRepo.save(testProduct);

        Product returnedProduct = productRepo.findById(testId);
        assertTrue("returned Product should be equal to saved Product",
                   returnedProduct.equals(testProduct));
    }

    @Test
    public void test_InMemProductRepo_saveAndFindAll() {
        int numProducts = 5;
        List<Product> createdProducts = new ArrayList<Product>();

        for(int i = 0; i < numProducts; ++i) {
            Product testProduct = new Product(testId + i, testName + i, testPrice + (i*999));
            productRepo.save(testProduct);
            createdProducts.add(testProduct);
        }

        List<Product> allProducts = productRepo.findAll();

        for(Product thisProduct: createdProducts) {
            assertTrue("Returned product List should contain all created Products",
                       allProducts.contains(thisProduct));
        }
    }
}
