package com.citi.training.products.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ProductTests {

    private int testId = 49;
    private String testName = "Beans";
    private double testPrice = 599.49;

    @Test
    public void test_Product_defaultConstructorAndSetters() {
        Product testProduct = new Product();

        testProduct.setId(testId);
        testProduct.setName(testName);
        testProduct.setPrice(testPrice);

        assertEquals("Product id should be equal to testId",
                     testId, testProduct.getId());
        assertEquals("Product name should be equal to testName",
                     testName, testProduct.getName());
        assertEquals("Product price should be equal to testPrice",
                     testPrice, testProduct.getPrice(), 0.00001);
    }

    @Test
    public void test_Product_fullConstructor() {
        Product testProduct = new Product(testId, testName, testPrice);

        assertEquals("Product id should be equal to testId",
                     testId, testProduct.getId());
        assertEquals("Product name should be equal to testName",
                     testName, testProduct.getName());
        assertEquals("Product price should be equal to testPrice",
                     testPrice, testProduct.getPrice(), 0.00001);
    }

    @Test
    public void test_Product_toString() {
        Product testProduct = new Product(testId, testName, testPrice);

        assertTrue("toString should contain testId",
                   testProduct.toString().contains(Integer.toString(testId)));
        assertTrue("toString should contain testName",
                testProduct.toString().contains(testName));
        assertTrue("toString should contain testPrice",
                testProduct.toString().contains(Double.toString(testPrice)));
    }

    @Test
    public void test_Product_equals() {
        Product firstProduct = new Product(testId, testName, testPrice);

        Product compareProduct = new Product(testId, testName, testPrice);
        assertFalse("products for test should not be the same object",
                    firstProduct == compareProduct);
        assertTrue("products for test should be found to be equal",
                   firstProduct.equals(compareProduct));
    }

    @Test
    public void test_Product_equalsFails() {
        Product firstProduct = new Product(testId + 1, testName, testPrice);
        Product secondProduct = new Product(testId, testName + "_test", testPrice);
        Product thirdProduct = new Product(testId, testName, testPrice * 99.99);
        Product[] createdProducts = {firstProduct, secondProduct, thirdProduct};

        Product compareProduct = new Product(testId, testName, testPrice);

        for(Product thisProduct: createdProducts) {
            assertFalse("products for test should not be the same object",
                        thisProduct == compareProduct);
            assertFalse("products for test should not be found to be equal",
                        thisProduct.equals(compareProduct));
        }
    }
}
