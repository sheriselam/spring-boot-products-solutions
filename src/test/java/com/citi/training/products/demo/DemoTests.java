package com.citi.training.products.demo;

import org.junit.Test;

import com.citi.training.products.repo.InMemoryProductRepository;

public class DemoTests {

    @Test
    public void test_Demo_runCompletes() {
        Demo demo = new Demo();
        demo.setProductRepo(new InMemoryProductRepository());
        demo.run(null);
    }
}
